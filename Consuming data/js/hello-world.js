// JavaScript Document
// Wait for PhoneGap to load
document.addEventListener("deviceready", onDeviceReady, false);

// PhoneGap is ready
function onDeviceReady() {
}

function twitterSearch() {
    var searchTermElem = $('#txtSearchTerm');
    
    console.log('http://search.twitter.com/search.json?q=' + searchTermElem.val());
    
    $.ajax({
        url: "http://search.twitter.com/search.json",
        contentType: "application/json; charset=utf-8",
        type: "GET",
        dataType: "jsonp",
        data: {
            q: searchTermElem.val()
        },
        success: function(data) {
                    var items = data.results;
                    var list = $('#twitterResults');            
                    list.html("");
                    $.each(items, function() {
                        list.append($(document.createElement('li')).html(this.from_user));
                    });},
        error: function(){ alert('error getting result'); }
    });
}